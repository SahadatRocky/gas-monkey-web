
import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Home',
    url: '/pages/home',
    icon: 'icon-speedometer'
  },
  {
    name: 'User Management',
    url: '/pages/user-management',
    icon: 'icon-user',
    children: [
      {
        name: 'My Profile',
        url: '/pages/user-management/profile'
      },
      {
        name: 'Create user',
        url: '/pages/user-management/user-create'
      },
      {
        name: 'Users List',
        url: '/pages/user-management/users'
      }

    ]
  },
  {
    name: 'Customer management',
    url: '/pages/customer-management',
    icon: 'icon-user',
    children: [
      {
        name: 'Customers List',
        url: '/pages/customer-management/customers'
      },
    ]
  },
  {
    name: 'Inventory management',
    url: '/pages/inventory-management',
    icon: 'icon-user',
    children: [
      {
        name: 'Category List',
        url: '/pages/inventory-management/category/categories'
      },
    ]
  },
];
