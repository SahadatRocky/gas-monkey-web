import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'home',
      loadChildren:  () => import('./home/home.module').then(m => m.HomeModule)
    },
    {
        path: 'user-management',
        loadChildren: () => import('./user-management/user-management.module').then(m => m.UserManagementModule)
    },
    {
      path: 'customer-management',
      loadChildren: () => import('./customer-management/customer-management.module').then(m => m.CustomerManagementModule)
    },
    {
      path: 'inventory-management',
      loadChildren: () => import('./inventory-management/inventory-management.module').then(m => m.InventoryManagementModule)
    },
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full'
    },
    {
      path: '**',
      component: NotFoundComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
