import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-auth',
  styleUrls: ['auth.component.scss'],
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AuthComponent {
  constructor(
    private router: Router,
  ) {
  }
}
