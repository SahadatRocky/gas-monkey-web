import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  formGroup: FormGroup;
  constructor(private fb: FormBuilder,
             private router : Router){}
  ngOnInit(): void {
    this.createFormGroup();
  }


  createFormGroup(){
    this.formGroup = this.fb.group({
      username: ['',  [Validators.required]],
      password: ['',Validators.required],
    });
  }

  

  forwardToLogin(){
    this.router.navigate(['/auth/login']);

  }

  get username() {
    return this.formGroup.get('username');
  }

  get password() {
    return this.formGroup.get('password');
  }

}
