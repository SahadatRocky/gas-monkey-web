import { Injectable } from "@angular/core";

import { BehaviorSubject, Observable } from "rxjs";
import { Router } from "@angular/router";
import { ApiEndpoints } from "../../../api-endpoints";
import { HttpClient } from "@angular/common/http";
import { Login } from "../../../containers/core/model/login";
import { map } from "rxjs/operators";
import { LocalStorageService } from "../../shared/service/local-storage-service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private apiEndpoints: ApiEndpoints = new ApiEndpoints();
  private _currentuser: BehaviorSubject<any> = new BehaviorSubject({});
  public readonly currentuser = this._currentuser.asObservable();
  constructor(
    private router: Router,
    private http: HttpClient,
    private localStorageService: LocalStorageService
  ) {}

  setCurrentUserData(data: any) {
    this._currentuser.next(data);
  }

  isLoggedIn(): boolean {
    if (this.localStorageService.getIsAuthenticated()) {
      return true;
    } else {
      return false;
    }
  }

  hasRole(roleName: string): boolean {
    let flag = 0;
    // this.keycloakAuth.realmAccess?.roles.forEach((element, i) => {
    //   if (element === roleName) {
    //     flag = 1;
    //   }
    // });
    if (flag === 1) {
      return true;
    } else {
      return false;
    }
  }

  login(loginObj: Login): Observable<any> {
    let url = this.apiEndpoints.LOGIN_URL;
    // const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.post<any>(url, loginObj).pipe(map((value) => value));
  }

 refreshJWTToken(refreshToken:any): Observable<any> {
   let url = this.apiEndpoints.REFRESH_TOKEN;
    return  this.http.post<any>(url, refreshToken).pipe(map((value) => value));
  }

  logout() {
    localStorage.clear();
    this._currentuser.next(null);
    this.router.navigate(["/auth/login"]);
  }

  // get isSuperAdmin() {
  //   if (this.user.username === "admin") {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // forgot(username: string, returnUrl: string): Observable<any> {
  //   return this.crudService.post('/v1/user/password/reset/request', { username, returnUrl });
  // }
  // validateResetToken(token): Observable<any> {
  //   return this.crudService.get('/v1/user/DEFAULT/reset/' + token);
  // }
  // resetPassword(token, param): Observable<any> {
  //   return this.crudService.post('/v1/user/DEFAULT/password/' + token, param);
  // }
  // checkIfStoreExist(code): Observable<any> {
  //   const params = {
  //     'code': code,
  //   };
  //   return this.crudService.get(`/v1/store/unique`, params);
  // }
  // register(param): Observable<any> {
  //   return this.crudService.post('/v1/store/signup', param)
  // }
}
