import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from '../../shared/service/local-storage-service';
import { StorageService } from '../../shared/service/storage.service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;
  constructor(private fb: FormBuilder,
             private authService: AuthService,
             private router : Router,
             private localStorageService: LocalStorageService){}
  ngOnInit(): void {
    this.createFormGroup();
  }

  createFormGroup(){
    this.formGroup = this.fb.group({
      username: ['',  [Validators.required]],
      password: ['',Validators.required],
    });
  }

  userLogin(){
    const username = this.formGroup.get('username').value;
    const password = this.formGroup.get('password').value;
    const obj = {
      "username": username,
      "password": password
    };

    this.authService.login(obj).subscribe(response =>{
       console.log(response);
       if(response){
        this.localStorageService.setIsAuthenticated(true);
        this.localStorageService.setJWTToken(response.token);
        this.localStorageService.setJWTRefreshToken(response.refreshToken);
        this.authService.setCurrentUserData(response);

        this.router.navigate(['/pages/home']);
       }else{

       }
    });
  }

  userForgotPassword(){
    this.router.navigate(['/auth/forgot-password']);

  }

  get username() {
    return this.formGroup.get('username');
  }

  get password() {
    return this.formGroup.get('password');
  }

}
