import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {

  ELEMENT_DATA: any = [
    { name: 'Cris', email: 'cris@gmail.com', status: 'active'},
    { name: 'Ponting', email: 'p@gmail.com', status: 'inactive'}
  ];

  public pageSizeOptions = [10,20,100,500];
  displayedColumns: string[];
  paginatorLength: number;
  pageEvent: PageEvent;
  dataSize = 10;
  isShowLoading = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource();
  constructor(private router: Router,) { }

  ngOnInit(): void {
    this.setApiColumns();
    this.setTableData(this.ELEMENT_DATA);

  }

  setApiColumns() {
    this.displayedColumns = ['name', 'email' , 'status', 'action'];
  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  onPaginateChange(pageEvent: PageEvent) {
    // this.getClientList(pageEvent.pageIndex, pageEvent.pageSize);
  }

  applyFilter(filterValue: string) {

    if(filterValue.length >= 3){
    //  this.apiBankHelperService.getClientTableDataForSearch(0, this.dataSize, filterValue).subscribe(
    //    res => {
    //      this.isShowLoading = false;
    //      this.setDashboardData(res.data.content);
    //      this.setPagination(res.data.totalPages, res.data.totalElements);
    //    },
    //    error => console.log(error)
    //  );
   }else if(filterValue.length == 0){
    //  this.getClientList(0, this.dataSize);
   }
 }

 createNewCustomer(){
  this.router.navigate(['/pages/customer-management/customer-edit/' + 0]);
 }

 sendRowDataForEditCustomer(event) {
  console.log(event);
  this.router.navigate(['/pages/customer-management/customer-edit/' + event.oid]);
}

}
