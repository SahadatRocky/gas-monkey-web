import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent implements OnInit {
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  billingForm: FormGroup;
  shippingForm: FormGroup;

  country: any = [
    {value: 'India', viewValue: 'India'},
    {value: 'Japan', viewValue: 'Japan'},
    {value: 'Canada', viewValue: 'Canada'},
   
  ];

  language: any = [
    {value: 'language1', viewValue: 'English'},
    {value: 'language2', viewValue: 'Bangla'},
  ];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.createBillingFormGroup();
    this.createShippingFormGroup();
  }

  createBillingFormGroup(){
    this.billingForm = this.fb.group({
      id: [''],
      firstName: ['',  [Validators.required]],
      lastName: ['',  [Validators.required]],
      userName: ['',  [Validators.required]],
      company: ['',  [Validators.required]],
      streetAddress: ['',  [Validators.required]],
      city: ['',  [Validators.required]],
      country: [''],
      stateProvince: ['',  [Validators.required]],
      postalCode: ['',  [Validators.required]],
      email: ['',[Validators.required, Validators.pattern(this.emailPattern)]],
      phoneNumber: ['',  [Validators.required]],
      language: [''],
     
    } );

  }
  createShippingFormGroup(){
    this.shippingForm = this.fb.group({
      id: [''],
      firstName: ['',  [Validators.required]],
      lastName: ['',  [Validators.required]],
      company: ['',  [Validators.required]],
      streetAddress: ['',  [Validators.required]],
      city: ['',  [Validators.required]],
      postalCode: ['',  [Validators.required]],
      country: [''],
      stateProvince: ['',  [Validators.required]],
      group: [''],
     
    } );

  }

  get firstName() {
    return this.billingForm.get('firstName');
  }
  get lastName() {
    return this.billingForm.get('lastName');
  }
  get userName() {
    return this.billingForm.get('userName');
  }
  get company() {
    return this.billingForm.get('company');
  }
  get streetAddress() {
    return this.billingForm.get('streetAddress');
  }
  get city() {
    return this.billingForm.get('city');
  }
  get stateProvince() {
    return this.billingForm.get('stateProvince');
  }
  get postalCode() {
    return this.billingForm.get('postalCode');
  }
  get email() {
    return this.billingForm.get('email');
  }
  get phoneNumber() {
    return this.billingForm.get('phoneNumber');
  }

}

