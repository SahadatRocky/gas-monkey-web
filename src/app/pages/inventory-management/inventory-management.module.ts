import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InventoryManagementRoutingModule } from './inventory-management-routing.module';
import { InventoryComponent } from './inventory/inventory.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    InventoryComponent
  ],
  imports: [
    CommonModule,
    InventoryManagementRoutingModule,
    SharedModule
  ]
})
export class InventoryManagementModule { }
