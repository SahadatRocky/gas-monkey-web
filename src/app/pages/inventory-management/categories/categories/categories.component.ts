import { Component, OnInit } from '@angular/core';
import { OBJ_EDIT, OBJ_LIST, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { EmitterService } from '../../../shared/service/emitter.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  mode: string;
  selectedCategoryObj: any;

  constructor(private emitterService: EmitterService) { }
  ngOnInit() {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }

  listenEmittedApiObj() {
    this.emitterService.selectedCategoryObj.subscribe(data => {
      if (data) {
        if (data.actionMode === OBJ_EDIT) {
          this.mode = OBJ_EDIT;
        } else if (data.actionMode === OBJ_NEW) {
          this.mode = OBJ_NEW;
        }  else {
          this.mode = OBJ_LIST;
        }
        this.selectedCategoryObj = data;
      } else {
        this.mode = OBJ_LIST;
      }
    });
  }

}
