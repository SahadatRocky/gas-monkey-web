import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { UtilService } from '../../../shared/service/util.service';
import { CategoryService } from '../service/category.service';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {

  ELEMENT_DATA: any = [
    // { name: 'Cris', email: 'cris@gmail.com', status: 'active'},
    // { name: 'Ponting', email: 'p@gmail.com', status: 'inactive'}
  ];

  public pageSizeOptions = [10,20,100,500];
  displayedColumns: string[];
  paginatorLength: number;
  pageEvent: PageEvent;
  dataSize = 10;
  isShowLoading = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource();
  constructor(
    private categoryService: CategoryService,
    private utilService: UtilService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.setApiColumns();
    this.getCategoriesList(0, this.dataSize);
  }

  setApiColumns() {
    this.displayedColumns = ['categoryName', 'merchantStore' ,'code', 'parent', 'status', 'action'];
  }

  getCategoriesList(page: number, size: number) {
    // this.isShowLoading = true;
    // this.categoryService.getCategoriesTableData(page,size).subscribe((res) =>{
    //   this.isShowLoading = false;
    //   console.log("----",res);
    //   this.setTableData(res.content);
    //   this.setPagination(res.totalPages, res.totalElements);
    // });
  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  setPagination(totalPages: number, totalElements: number) {
    this.paginatorLength = totalElements;
  }

  onPaginateChange(pageEvent: PageEvent) {
    this.getCategoriesList(pageEvent.pageIndex, pageEvent.pageSize);
  }

  sendRowData(event) {
    console.log(event);
    this.router.navigate(['/pages/inventory-management/category/categories/' + event.id]);
  }

  applyFilter(filterValue: string) {

    if(filterValue.length >= 3){
    //  this.apiBankHelperService.getClientTableDataForSearch(0, this.dataSize, filterValue).subscribe(
    //    res => {
    //      this.isShowLoading = false;
    //      this.setDashboardData(res.data.content);
    //      this.setPagination(res.data.totalPages, res.data.totalElements);
    //    },
    //    error => console.log(error)
    //  );
   }else if(filterValue.length == 0){
    //  this.getClientList(0, this.dataSize);
   }
 }

}
