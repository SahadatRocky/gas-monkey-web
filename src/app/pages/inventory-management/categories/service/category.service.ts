import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ApiEndpoints } from "../../../../api-endpoints";


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

//   HTTPOptions:Object = {
//     observe: 'response',
//     responseType: 'text'
//  }

  private apiEndpoints: ApiEndpoints = new ApiEndpoints();
  constructor(private http: HttpClient) { }


  getCategoriesTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  // getUserById(id:any): Observable<any>{
  //   let url = this.apiEndpoints.USER_BY_ID + '/' + id;
  //   return this.http.get<any>(url).pipe(
  //     map(value => value)
  //     );
  // }

}
