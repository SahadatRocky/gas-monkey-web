import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InventoryComponent } from './inventory/inventory.component';

const routes: Routes = [
  {
    path: '',
  component: InventoryComponent,
  children: [
    {
      path: 'category',
      loadChildren: ()=> import('./categories/categories.module').then(e=>e.CategoriesModule)
    },
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryManagementRoutingModule { }
