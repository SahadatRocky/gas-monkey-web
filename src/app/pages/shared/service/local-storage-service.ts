import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  constructor(private storageService: StorageService) {
  }


  getJWTToken(): string{
    return this.storageService.read('jwt_token');
  }

  getJWTRefreshToken(): string{
    return this.storageService.read('jwt_refresh_token');
  }

  setJWTToken(jwttoken: string):void{
    this.storageService.save('jwt_token', jwttoken);
  }

  setJWTRefreshToken(refreshtoken: string): void{
    this.storageService.save('jwt_refresh_token', refreshtoken);
  }

  setIsAuthenticated(authenticated:any){
    this.storageService.save('isAuthenticated', authenticated);
  }

  getIsAuthenticated(): string{
    return this.storageService.read('isAuthenticated');
  }

}
