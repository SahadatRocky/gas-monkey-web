import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmitterService {

  selectedUserObj: EventEmitter<any> = new EventEmitter<any>();
  selectedCustomerObj: EventEmitter<any> = new EventEmitter<any>();
  selectedCategoryObj: EventEmitter<any> = new EventEmitter<any>();
  constructor() {
  }

  emitSelectedUserObj(selectedObj: any) {
    this.selectedUserObj.emit(selectedObj);
  }

  emitSelectedCustomerObj(selectedObj: any) {
    this.selectedCustomerObj.emit(selectedObj);
  }

  emitSelectedCategoryObj(selectedObj: any) {
    this.selectedCategoryObj.emit(selectedObj);
  }

}
