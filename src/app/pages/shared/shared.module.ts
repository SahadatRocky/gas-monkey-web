import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';




@NgModule({
  declarations: [
    NotFoundComponent,
    
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],exports:[
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class SharedModule {
 
  
 }
