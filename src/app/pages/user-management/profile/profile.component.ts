import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { UserService } from '../service/user.service';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  userForm: FormGroup;
  controlArray:any;
  groupsList:any;
  merchantStore: any = [
    {value: '/pages/user-management/profile', viewValue: 'profile'},
    {value: '/pages/user-management/change-password', viewValue: 'Change Password'}

  ];
  merchantStore1: any = [
    {value: 'merchant1', viewValue: 'Default'},
    {value: 'merchant2', viewValue: 'Merchant1'},
    {value: 'merchant3', viewValue: 'Merchant3'},

  ];

  language:any = [
    {value: 'en', viewValue: 'English'},
    {value: 'bn', viewValue: 'Bangla'},
  ];

  orders = [
    {id: 'superAdmin', name: "Super Admin"},
    {id: 'admin', name: "Admin"},
    {id: 'adminRetailer', name: "Admin Retailer"},
    {id: 'adminStore', name: "Admin Store"},
    {id: 'adminCatalogue', name: "Admin Catalogue"},
    {id: 'adminOrder', name: "Admin Order"},
    {id: 'adminContent', name: "Admin Content"},
  ];

  constructor(private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService) {
    this.controlArray = this.orders.map(c => new FormControl(false));
  }

  ngOnInit(): void {
    this.createFormGroup();
    this.getGroupDropdownListData();

  }

  createFormGroup(){
    this.userForm = this.fb.group({
      id: [''],
      firstName: ['',  [Validators.required]],
      lastName: ['',  [Validators.required]],
      merchantStore: [''],
      email: ['',[Validators.required, Validators.pattern(this.emailPattern)]],
      language:[''],
      orders: new FormArray(this.controlArray),
      group: ['']
    },{
     // validator: MustMatch('password', 'repeatPassword')
    }
    );
  }

  getGroupDropdownListData(){
    this.userService.getGroupDropdownList().subscribe(response =>{
      this.groupsList = response;
      //console.log('-->>',response);
    });
}

  get firstName() {
    return this.userForm.get('firstName');
  }
  get lastName() {
    return this.userForm.get('lastName');
  }

  getOrders() {
    return (this.userForm.get('orders') as FormArray).controls;
  }

  get email() {
    return this.userForm.get('email');
  }

  onSubmit(){

  }

  onChange(event:any){
     console.log(event.value);
     if(event.value==='profile'){
      this.router.navigateByUrl('/pages/user-management/profile');
     }else{
      this.router.navigateByUrl('/pages/user-management/change-password');
     }
     // this.router.navigateByUrl('/pages/user-management/change-password');

  }



}
