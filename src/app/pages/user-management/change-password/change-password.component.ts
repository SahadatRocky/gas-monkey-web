import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  userForm: FormGroup;
  merchantStore: any = [
    {value: 'marchant1', viewValue: 'My Profile'},
    // {value: ['/profile'], viewValue: 'My Profile'},
    
   
  ];

  constructor(private activatedRoute: ActivatedRoute, private fb: FormBuilder,private router: Router,
    ) { }

  ngOnInit(): void {
    this.createFormGroup();
  }
  createFormGroup(){
    this.userForm = this.fb.group({
      id: [''],
      currentPassword: ['',[Validators.required]],
      password: ['',[Validators.required,Validators.minLength(3)]],
      repeatPassword: ['',[Validators.required]],
     
     
    },{
      //validator: MustMatch('password', 'repeatPassword')
    }
    );
  }

  onOptionsSelected(event:any) {
    console.log(event);
 }

  get currentPassword(){
    return this.userForm.get('currentPassword');
  }
  get password(){
    return this.userForm.get('password');
  }
  get repeatPassword(){
    return this.userForm.get('repeatPassword');
  }

  onChange(event:any){
    console.log(event.value);
    if(event.value==='profile'){
     this.router.navigateByUrl('/pages/user-management/profile');
    }else{
     this.router.navigateByUrl('/pages/user-management/change-password');
    }
    // this.router.navigateByUrl('/pages/user-management/change-password');
   
 }



}
