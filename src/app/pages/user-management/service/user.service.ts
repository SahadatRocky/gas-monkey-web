import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ApiEndpoints } from "../../../api-endpoints";


@Injectable({
  providedIn: 'root'
})
export class UserService {

  HTTPOptions:Object = {
    observe: 'response',
    responseType: 'text'
 }

  private apiEndpoints: ApiEndpoints = new ApiEndpoints();
  constructor(private http: HttpClient) { }

  getGroupDropdownList(): Observable<any>{
    let url = this.apiEndpoints.GROUP_DROPDOWN_LIST;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getLanguageDropdownList(): Observable<any>{
    let url = this.apiEndpoints.LANGUAGE_DROPDOWN_LIST;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  UserRegistration(Obj: any): Observable<any>{
    let url = this.apiEndpoints.USER_REGISTRATION;
    return this.http.post<any>(url, Obj,this.HTTPOptions).pipe(map(value => value))
  }

  getUsersTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.USERS_LIST;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  getUserById(id:any): Observable<any>{
    let url = this.apiEndpoints.USER_BY_ID + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  userUpdate(id:any, obj:any): Observable<any>{
    let url = this.apiEndpoints.USER_UPDATE + '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
    );
  }

}
