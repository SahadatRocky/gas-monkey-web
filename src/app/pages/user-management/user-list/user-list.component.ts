import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { UtilService } from '../../shared/service/util.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  ELEMENT_DATA: any = [
    // { name: 'Cris', email: 'cris@gmail.com', status: 'active'},
    // { name: 'Ponting', email: 'p@gmail.com', status: 'inactive'}
  ];

  public pageSizeOptions = [10,20,100,500];
  displayedColumns: string[];
  paginatorLength: number;
  pageEvent: PageEvent;
  dataSize = 10;
  isShowLoading = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource();
  constructor(private userService: UserService,
    private utilService: UtilService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.setApiColumns();
    this.getUsersList(0, this.dataSize);
  }

  setApiColumns() {
    this.displayedColumns = ['name', 'email' ,'mobile', 'status', 'action'];
  }

  getUsersList(page: number, size: number) {
    this.isShowLoading = true;
    this.userService.getUsersTableData(page,size).subscribe((res) =>{
      this.isShowLoading = false;
      console.log("----",res);
      this.setTableData(res.content);
      this.setPagination(res.totalPages, res.totalElements);
    });
  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  setPagination(totalPages: number, totalElements: number) {
    this.paginatorLength = totalElements;
  }

  onPaginateChange(pageEvent: PageEvent) {
    this.getUsersList(pageEvent.pageIndex, pageEvent.pageSize);
  }

  sendRowData(event) {
    console.log(event);
    this.router.navigate(['/pages/user-management/user-edit/' + event.id]);
  }

  applyFilter(filterValue: string) {

    if(filterValue.length >= 3){
    //  this.apiBankHelperService.getClientTableDataForSearch(0, this.dataSize, filterValue).subscribe(
    //    res => {
    //      this.isShowLoading = false;
    //      this.setDashboardData(res.data.content);
    //      this.setPagination(res.data.totalPages, res.data.totalElements);
    //    },
    //    error => console.log(error)
    //  );
   }else if(filterValue.length == 0){
    //  this.getClientList(0, this.dataSize);
   }
 }

}
