import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { UtilService } from '../../shared/service/util.service';
import { MustMatch } from '../../shared/validation/must-match.validator';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  @Input() selectedUserObj: any;
  userForm: FormGroup;
  userId:any;
  userData:any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  groupsList:any;
  languageList:any;
  languageChange:any;
  groupChange:any;

  merchantStore: any = [
    {value: 'merchant1', viewValue: 'merchant 1'},
    {value: 'merchant2', viewValue: 'merchant 2'},
    {value: 'merchant3', viewValue: 'merchant 3'},
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private userService: UserService,
    private utilService: UtilService) {

      // this.controlArray[0].setValue(false);
     }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params => {
        this.userId = params.id;
        console.log(this.userId);
        this.buttonText = this.userId != '0' ? 'UPDATE' : 'SAVE';
        this.actionMode = this.userId != '0' ? OBJ_EDIT : OBJ_NEW;
      });

      if(this.actionMode == OBJ_EDIT){
        this.getDataUserById();
        this.getGroupDropdownListData();
        this.getLanguageDropdownListData();
        //this.userForm.controls['email'].disable();
      }else{
        // this.isShowLoading = false;
        // this.userForm.controls['userId'].enable();
      }

    this.createFormGroup();

  }

  createFormGroup(){
    this.userForm = this.fb.group({
      firstName: ['',  [Validators.required]],
      lastName: ['',  [Validators.required]],
      merchantId: [''],
      email: ['',[Validators.required, Validators.pattern(this.emailPattern)]],
      mobile:['', [Validators.required]],
      languageId:[''],
      groupId: [''],
      active:[false]
    }
    );
  }

  getDataUserById(){
    this.isShowLoading = true;
    this.userService.getUserById(this.userId).subscribe(res => {
      console.log(res);
      this.userData = res;
      this.isShowLoading = false;
      this.populateData(res);
    })
  }


  getGroupDropdownListData(){
       this.userService.getGroupDropdownList().subscribe(response =>{
         this.groupsList = response;
         console.log('-->>',response);
       });
  }

  getLanguageDropdownListData(){
    this.userService.getLanguageDropdownList().subscribe(response =>{
      this.languageList = response;
       console.log('-->>',response);
    });
  }

  populateData(myData) {
    this.userForm.patchValue(myData);
    this.userForm.get('languageId').setValue(myData.languageBean.id);
    this.userForm.get('groupId').setValue(myData.groupInfoBean.id);
    this.userForm.controls['email'].disable();
  }

  onSubmit(): void {

    this.languageChange = this.languageList.filter(e => e.id == this.userForm.get('languageId').value);
    this.groupChange = this.groupsList.filter(e => e.id == this.userForm.get('groupId').value );
    let obj = {
      "id": this.userId,
      "firstName": this.userForm.value.firstName,
      "lastName": this.userForm.value.lastName,
      "mobile": this.userForm.value.mobile,
      "active": this.userForm.value.active,
      "languageBean": {
          "id": this.languageChange[0].id,
          "title": this.languageChange[0].value,
          "code": this.languageChange[0].code,
          "sortOrder": 1
      },
      "groupInfoBean": {
          "id": this.groupChange[0].id,
          "groupName": "",
          "groupType": ""
      },
      "merchantId": this.userForm.value.merchantId
  };

  this.userService.userUpdate(this.userId,obj).subscribe(res =>{
    console.log(res);
    this.populateData(res);
    this.utilService.showSnackBarMessage('User Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
  },error => {
    this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
  });
}

get email() {
  return this.userForm.get('email');
}

get firstName() {
  return this.userForm.get('firstName');
}

get lastName() {
  return this.userForm.get('lastName');
}

get password(){
  return this.userForm.get('password');
}

get mobile(){
  return this.userForm.get('mobile');
}

get confirmPassword(){
  return this.userForm.get('confirmPassword');
}
}
