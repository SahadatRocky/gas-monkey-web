import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { MustMatch } from '../../shared/validation/must-match.validator';
import { UserService } from '../service/user.service';
import { v4 as uuidv4 } from 'uuid';
import { UtilService } from '../../shared/service/util.service';
@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  @Input() selectedUserObj: any;
  userForm: FormGroup;
  useroId:any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  groupsList:any;
  languageList:any;

  merchantStore: any = [
    {value: 'merchant1', viewValue: 'merchant 1'},
    {value: 'merchant2', viewValue: 'merchant 2'},
    {value: 'merchant3', viewValue: 'merchant 3'},
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private utilService: UtilService
    ) {
      // this.controlArray[0].setValue(false);
     }

  ngOnInit(): void {

    this.createFormGroup();
    this.getGroupDropdownListData();
    this.getLanguageDropdownListData();

  }

  createFormGroup(){
    this.userForm = this.fb.group({
      firstName: ['',  [Validators.required]],
      lastName: ['',  [Validators.required]],
      merchantId: [''],
      email: ['',[Validators.required, Validators.pattern(this.emailPattern)]],
      password: ['',[Validators.required,Validators.minLength(8)]],
      confirmPassword: ['',[Validators.required]],
      mobile:[''],
      languageId:[''],
      groupId: ['']
    },{
      validator: MustMatch('password', 'confirmPassword')
    }
    );
  }

  getGroupDropdownListData(){
       this.userService.getGroupDropdownList().subscribe(response =>{
         this.groupsList = response;
         //console.log('-->>',response);
       });
  }

  getLanguageDropdownListData(){
    this.userService.getLanguageDropdownList().subscribe(response =>{
      this.languageList = response;
      //console.log('-->>',response);
    });
  }

  onSubmit(): void {
    let uuid = uuidv4();
    console.log(uuid);
    this.userForm.controls.merchantId.setValue(uuid);
    let obj = {
      "firstName": this.userForm.value.firstName,
      "lastName": this.userForm.value.lastName,
      "password": this.userForm.value.password,
      "confirmPassword": this.userForm.value.confirmPassword,
      "mobile":this.userForm.value.mobile,
      "groupId": this.userForm.value.groupId,
      "languageId": this.userForm.value.languageId,
      "merchantId":this.userForm.value.merchantId,
      "email":this.userForm.value.email,
  }
  this.userService.UserRegistration(obj).subscribe(res=>{
    if ( res['status'] < 300 && res['status'] >= 200) {
      this.utilService.showSnackBarMessage('User Registration Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
      this.router.navigate(['/pages/user-management/users']);
    }
  },error => {
    this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
  });
}


get email() {
  return this.userForm.get('email');
}

get firstName() {
  return this.userForm.get('firstName');
}

get lastName() {
  return this.userForm.get('lastName');
}

get password(){
  return this.userForm.get('password');
}

get mobile(){
  return this.userForm.get('mobile');
}

get confirmPassword(){
  return this.userForm.get('confirmPassword');
}
}
