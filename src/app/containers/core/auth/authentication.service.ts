import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { User } from '../model/user';


let users=[new User('admin','123'),  new User('user','123')];
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

user = new User;
currentuser: BehaviorSubject<string> = new BehaviorSubject<string>(this.getCurrentUser());
constructor(private router: Router) { }


getCurrentUser() {
    return localStorage.getItem('user');
  }

isLoggedIn() : boolean {
   if(localStorage.getItem('isAuthenticated') == 'true'){
       return true;
   }else{
     return false;
   }
  }

  /**
   * Checks if the logged user has the role specified
   *
   * @param roleName The name of the role
   * @param resource The keycloak client
   */
  hasRole(roleName: string): boolean {
    let flag = 0;
    // this.keycloakAuth.realmAccess?.roles.forEach((element, i) => {
    //   if (element === roleName) {
    //     flag = 1;
    //   }
    // });
    if (flag === 1) {
      return true;
    } else {
      return false;
    }
  }


get isSuperAdmin() {
    if(this.user.username==="admin"){
                              return true;
                             }else
                              {
                              return false;
                              }
                              }



login(username:string,password:string){
  this.user.username=username;
  this.user.password=password;

    let authenticatedUser = users.find(u => u.username === this.user.username);
    if( authenticatedUser && authenticatedUser.password==this.user.password ){
    localStorage.setItem("isAuthenticated", "true");
    localStorage.setItem("user", this.user.username);
    this.currentuser.next(this.user.username);
    return true;
   }
  else{
    return false;
  }
 }

 logout(){
   localStorage.clear();
   this.currentuser.next(null);
   this.router.navigate(['/auth/login']);
 }

}
