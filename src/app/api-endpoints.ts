export class ApiEndpoints {
  ROOT_URL = 'https://testapi-k8s.oss.net.bd/gas-monkey'; //UAT
  // ROOT_URL = 'http://192.168.30.83:8801/api'; // for DEV SERVER


  //End-Point

  LOGIN_URL = `${this.ROOT_URL}/api/v1/auth/login`;
  REFRESH_TOKEN = `${this.ROOT_URL}/api/v1/auth/refresh-token`;
  GROUP_DROPDOWN_LIST = `${this.ROOT_URL}/api/v1/admin/group-info/dropdown-list`;
  LANGUAGE_DROPDOWN_LIST = `${this.ROOT_URL}/api/v1/admin/language/dropdown-list`;
  USER_REGISTRATION = `${this.ROOT_URL}/api/v1/admin/user-registration`;
  USERS_LIST = `${this.ROOT_URL}/api/v1/admin/users`;
  USER_BY_ID = `${this.ROOT_URL}/api/v1/admin/users`;
  USER_UPDATE = `${this.ROOT_URL}/api/v1/admin/users`;
  CATALOG_LIST =  `${this.ROOT_URL}/api/v1/admin/catalog`;
}
